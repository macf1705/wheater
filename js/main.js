var apiKey = ""; // https://www.weatherbit.io register here to get api key
var cityName;

let dayOfWeek = (inputDate) => {
    let days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    let date = new Date(inputDate);

    return days[date.getDay()];
};

let showError = (message) => {
    var node = document.createElement("p");
    node.setAttribute("id", "error-message");
    document.getElementById("forecast").appendChild(node);
    document.getElementById("error-message").innerHTML = message;
};

let getData = (url) => {
    fetch(url)
        .then(res => res.json())
        .then(response => {
            generateMarkup(response.data);
        })
        .catch(err => {
            showError("Sorry, try again.");
        });
};

let geoLocate = () => {
    let success = (position) => {
        let url = "https://api.weatherbit.io/v2.0/forecast/daily?&lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&days=5&key=" + apiKey;

        getData(url);
    }

    let error = () => {
        showError("Sorry, your browser doesnt support geolocation.");
    }

    let options = {
        enableHighAccuracy: true,
        maximumAge: 30000,
        timeout: 27000
    }

    navigator.geolocation.getCurrentPosition(success, error, options);
};

if("geolocation" in navigator && location.protocol == "https:") {
    geoLocate();
}

let showByCityName = () => {
    cityName = document.getElementById("city-name").value;
    let url = "https://api.weatherbit.io/v2.0/forecast/daily?city=" + cityName.replace(/\s/g, '') + "&days=5&key=" + apiKey;

    getData(url);
};

let runSearchByEnter = (event) => {
    if(event.keyCode == 13) {
        showByCityName();
    }
}

let generateMarkup = (inputData) => {
    let markup = `${inputData.map(data => `
        <div class="day">
            <p class="date">${dayOfWeek(data.datetime)}</p>
            <p class="avg-temperature">${parseInt(data.temp)}°</p>
            <p class="weather-status"><i class="climacon ${data.weather.icon}"></i>${data.weather.description}</p>

            <div class="min-max-temperature">
                <span class="max-temperature"><i class="arrow-up"></i>${parseInt(data.max_temp)}°</span>
                <span class="min-temperature"><i class="arrow-down"></i>${parseInt(data.min_temp)}°</span>
            </div>

            <div class="details">
                <div class="details-row"><div class="details-col-1">Feels like</div><div class="details-col-2">${parseInt(data.app_max_temp)}°</div></div>
                <div class="details-row"><div class="details-col-1">Humidity</div><div class="details-col-2">${data.rh}%</div></div>
                <div class="details-row"><div class="details-col-1">Visibility</div><div class="details-col-2">${parseInt(data.vis)} km</div></div>
                <div class="details-row"><div class="details-col-1">UV index</div><div class="details-col-2">${parseInt(data.uv)}</div></div>
                <div class="details-row"><div class="details-col-1">Wind</div><div class="details-col-2">${parseFloat(data.wind_spd).toFixed(2)} m/s ${data.wind_cdir}</div></div>
                <div class="details-row"><div class="details-col-1">Barometer</div><div class="details-col-2">${parseInt(data.pres)} mBar</div></div>
            </div>
        </div>
    `).join('')}`;

    document.getElementById("forecast").innerHTML = "";
    document.getElementById("forecast").innerHTML = markup;
};
