/*eslint-env node */

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var eslint = require('gulp-eslint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');
var webpack = require('webpack-stream');
var named = require('vinyl-named');
var through = require('through2');
var runSequence = require('run-sequence');
var clean = require('gulp-clean');

gulp.task('default', ['copy-html', 'styles', 'lint', 'scripts'], function() {
	gulp.watch('sass/**/*.scss', ['styles']);
	gulp.watch('js/**/*.js', ['lint']);
	gulp.watch('/index.html', ['copy-html']);
	gulp.watch('./dist/index.html').on('change', browserSync.reload);

	browserSync.init({
		server: './dist'
	});
});

gulp.task('dist', ['copy-html','styles', 'lint', 'scripts-dist']);

gulp.task('scripts', function() {
	gulp.src('js/**/*.js')
		.pipe(babel())
		.pipe(concat('script.js'))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('scripts-temp', function() {
	return gulp.src('js/**/*.js')
		.pipe(babel())
		.pipe(concat('script.js'))
		.pipe(gulp.dest('temp'));
});

gulp.task('scripts-maps-sub', function() {
	return gulp.src('temp/script.js')
		.pipe(named())
		.pipe(webpack({
			devtool: 'source-map'
		}))
		.pipe(gulp.dest('temp'));
});

gulp.task('scripts-maps', function() {
	return gulp.src(['temp/script.js'])
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(uglify())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('clean-temp', function () {
	return gulp.src('temp', {read: false})
		.pipe(clean());
});

gulp.task('scripts-dist', function(callback) {
	runSequence('scripts-temp', 'scripts-maps-sub', 'scripts-maps', 'clean-temp', callback);
});

gulp.task('copy-html', function() {
	gulp.src('./index.html')
		.pipe(gulp.dest('./dist'));
});

gulp.task('styles', function() {
	gulp.src('sass/**/*.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.stream());
});

gulp.task('lint', function () {
	return gulp.src(['js/**/*.js'])
		// eslint() attaches the lint output to the eslint property
		// of the file object so it can be used by other modules.
		.pipe(eslint())
		// eslint.format() outputs the lint results to the console.
		// Alternatively use eslint.formatEach() (see Docs).
		.pipe(eslint.format())
		// To have the process exit with an error code (1) on
		// lint error, return the stream and pipe to failOnError last.
		.pipe(eslint.failOnError());
});
