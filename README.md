**How to run project**

To open project and work on it you will be need two things:
1. node [download here](https://nodejs.org/en/),
2. key for weather API, to get this you must register on [this website](https://www.weatherbit.io).

---

## Run and build

To run script you must install all dependencies needed for this project. To do that follow by instruction below.

1. [Download](https://bitbucket.org/macf1705/wheater/src/master/) repository with project .
2. Go to the the root project folder.
3. Open terminal and type **npm install**.
4. After that to buld final project type in console **gulp**
5. Now in folder *Dist* you will find result files.

---

## Build finall version - for server

To buld finall version open your terminal and type **gulp dist** after that copy everything from dist folder to your server

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

## Things to do in future

1. Add random image from city using flcker api and geocoordinates,
2. Add more info about wheater,
3. Add animation - example on load data from api, animated icons etc.
